;;;; -*- Mode: Lisp -*-

;;;; common-math.asd
;;;;
;;;; See file COPYING in the main folder for licensing information.


(asdf:defsystem "COMMON-MATH"
  :author "Marco Antoniotti"

  :license "BSD"

  :description "The COMMON-MATH System.

This library provides a layer of generic functions and some numerical
hooks that can be used to further \"extend\" Common Lisp mathematical
capabilities."

  :components ((:file "common-math-package")

               (:file "common-math-user-package"
                :depends-on ("common-math-package"))

               (:file "prologue"
                :depends-on ("common-math-package"))

               (:module "impl-dependent"
                :depends-on ("prologue" "common-math-package")
                :components (
                             #+lispworks
                             (:module "lispworks"
                              :components ((:file "lispworks")))

                             #+cmucl
                             (:module "cmucl"
                              :components ((:file "cmucl")))

                             #+sbcl
                             (:module "sbcl"
                              :components ((:file "sbcl")))

                             #+ccl
                             (:module "ccl"
                              :components ((:file "ccl")))

                             #+allegro
                             (:module "acl"
                              :components ((:file "acl")))

                             #-(or lispworks cmucl sbcl ccl allegro)
                             (:file "no-impl-dependent-code")
                             ))

               (:file "ieee-base"
                :depends-on ("common-math-package" "impl-dependent"))

               (:file "numerics-base"
                :depends-on ("common-math-package"))

               (:file "common-math"
                :depends-on ("common-math-package"
                             "ieee-base"
                             "numerics-base"))

               (:file "utilities"
                :depends-on ("common-math"))
               )
  )


;;;; end of file -- common-math.asd
