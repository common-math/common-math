;;;; -*- Mode: Lisp -*-

;;;; common-math-user-package.lisp
;;;;
;;;; See file COPYING in the main folder for licensing information.

(defpackage "IT.UNIMIB.DISCO.MA.CL.EXT.MATH-USER" (:use "CL" "COMMON-MATH")
  (:documentation "The Common Math User Common Lisp Package.

A 'user' package that can be used as a sandbox (sort of).
")

  (:nicknames
   "COMMON-MATH-USER"
   "MATH-USER"
   "CL.EXTENSIONS.MATH-USER"
   "CL.EXT.MATH-USER")

  (:shadowing-import-from "COMMON-MATH"
   "=" "+" "-" "*" "/" ">" "<" ">=" "<=" "/=")

  (:shadowing-import-from "COMMON-MATH"
   "PLUSP" "MINUSP" "ZEROP")

  (:shadowing-import-from "COMMON-MATH"
   "EXP" "EXPT" "GCD" "LCM")
  )

;;;; end of file -- common-math-user-package.lisp
