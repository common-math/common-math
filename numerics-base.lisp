;;;; -*- Mode: Lisp -*-

;;;; numerics-base.lisp
;;;;
;;;; See file COPYING in the main folder for licensing information.

(in-package "CL.EXT.MATH")

(declaim (ftype (function (fixnum fixnum) ratio) unit-roundoff))

(eval-when (:load-toplevel :compile-toplevel :execute)

(defun unit-roundoff (base precision)
  "Returns the numerical unit roundoff available on the platform.

BASE is \"beta\" and PRECISION is \"p\" in the standard Numerical
Analisys terminology."

  (declare (type fixnum base precision)) ; This should do it.
  (cl:* 1/2 (cl:expt base (cl:- 1 precision))))
)


#-sbcl
(progn

(defconstant +unit-roundoff-short-rational+
  (unit-roundoff 2 (float-precision 1.0s0)))

(defconstant +unit-roundoff-single-rational+
  (unit-roundoff 2 (float-precision 1.0f0)))

(defconstant +unit-roundoff-double-rational+
  (unit-roundoff 2 (float-precision 1.0d0)))

(defconstant +unit-roundoff-long-rational+
  (unit-roundoff 2 (float-precision 1.0l0)))


(defconstant +unit-roundoff-rational+ +unit-roundoff-double-rational+)


(defconstant +unit-roundoff-short-float+
  (cl:float +unit-roundoff-short-rational+ 1.0S0))

(defconstant +unit-roundoff-single-float+
  (cl:float +unit-roundoff-single-rational+ 1.0F0))

(defconstant +unit-roundoff-double-float+
  (cl:float +unit-roundoff-double-rational+ 1.0D0))

(defconstant +unit-roundoff-long-float+
  (cl:float +unit-roundoff-long-rational+ 1.0L0))


(defconstant +unit-roundoff-float+ +unit-roundoff-double-float+)
)


#+sbcl ; Fascist SBCL!!!!
(progn

(defparameter +unit-roundoff-short-rational+
  (unit-roundoff 2 (float-precision 1.0s0)))

(defparameter +unit-roundoff-single-rational+
  (unit-roundoff 2 (float-precision 1.0f0)))

(defparameter +unit-roundoff-double-rational+
  (unit-roundoff 2 (float-precision 1.0d0)))

(defparameter +unit-roundoff-long-rational+
  (unit-roundoff 2 (float-precision 1.0l0)))


(defparameter +unit-roundoff-rational+ +unit-roundoff-double-rational+)


(defparameter +unit-roundoff-short-float+
  (cl:float +unit-roundoff-short-rational+ 1.0S0))

(defparameter +unit-roundoff-single-float+
  (cl:float +unit-roundoff-single-rational+ 1.0F0))

(defparameter +unit-roundoff-double-float+
  (cl:float +unit-roundoff-double-rational+ 1.0D0))

(defparameter +unit-roundoff-long-float+
  (cl:float +unit-roundoff-long-rational+ 1.0L0))


(defparameter +unit-roundoff-float+ +unit-roundoff-double-float+)
)

;;;; end of file -- numerics-base.lisp
