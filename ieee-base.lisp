;;;; -*- Mode: Lisp -*-

;;;; ieee-base.lisp
;;;;
;;;; See file COPYING in the main folder for licensing information.


(in-package "CL.EXT.MATH")

(deftype rounding-modes ()
  "The Rounding Modes Union Type.

Notes:

Currently unused."

  '(member :positive ; C99 FE_UPWARD
           :negative ; C99 FE_DOWNWARD
           :zero     ; C99 FE_TOWARDZERO
           :nearest  ; C99 FE_TONEAREST
           ))


(defparameter *error-on-nan-returning-operations* nil
  "If non nil an error is signalled by operations that would return a NaN.

The error signalled is of type FLOATING-POINT-INVALID-OPERATION.")


;;;; end of file -- ieee-base.lisp
