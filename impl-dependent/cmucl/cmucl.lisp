;;;; -*- Mode: Lisp -*-

;;;; cmucl.lisp
;;;; Implementation dependent code.
;;;;
;;;; See file COPYING in the main folder for licensing information.


(in-package "CL.EXT.MATH")


(defparameter nan (kernel:make-double-float -524288 0)) ; Courtesy of Raymond Toy.
  


(defconstant long-float-positive-infinity ext:long-float-positive-infinity)
(defconstant long-float-negative-infinity ext:long-float-negative-infinity)

(defconstant double-float-positive-infinity ext:double-float-positive-infinity)
(defconstant double-float-negative-infinity ext:double-float-negative-infinity)

(defconstant single-float-positive-infinity ext:single-float-positive-infinity)
(defconstant single-float-negative-infinity ext:single-float-negative-infinity)

(defconstant short-float-positive-infinity ext:short-float-positive-infinity)
(defconstant short-float-negative-infinity ext:short-float-negative-infinity)


;;;; end of file -- cmucl.lisp
