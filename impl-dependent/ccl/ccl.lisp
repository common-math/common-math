;;;; -*- Mode: Lisp -*-

;;;; ccl.lisp
;;;; Implementation dependent code.
;;;;
;;;; See file COPYING in the main folder for licensing information.


(in-package "CL.EXT.MATH")


(defconstant nan 1D+-0)

(defconstant long-float-positive-infinity 1L++0)
(defconstant long-float-negative-infinity -1L++0)

(defconstant double-float-positive-infinity 1D++0)
(defconstant double-float-negative-infinity -1D++0)

(defconstant single-float-positive-infinity 1F++0)
(defconstant single-float-negative-infinity -1F++0)

(defconstant short-float-positive-infinity 1S++0)
(defconstant short-float-negative-infinity -1S++0)

;;;; end of file -- ccl.lisp
