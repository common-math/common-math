;;;; -*- Mode: Lisp -*-

;;;; no-impl-dependent-code.lisp
;;;;
;;;; See file COPYING in the main folder for licensing information.


(in-package "CL.EXT.MATH")

(eval-when (:load-toplevel :compile-toplevel :execute)
  (warn "CL.EXT.MATH: either no implementation dependente code has ~@
         been written for your Common Lisp implementation, ~A, ~@
         or your implementations does not support a number of numerical features ~@
         necessary for the Mathematical extensions to work.~@
         In this last case, most likely, IEEE NaNs and infinities are not accessible ~@
         in the implementation.~@
         ~@
         Many features will not work and many functions, variables and constants ~@
         will be undefined."
	(lisp-implementation-type)
	))
         

;;;; end of file -- no-impl-dependent-code.lisp
