;;;; -*- Mode: Lisp -*-

;;;; lispworks.lisp
;;;; Implementation dependent code.

(in-package "CL.EXT.MATH")


(defconstant nan 1D+-0)

(defconstant long-float-positive-infinity 1L++0)
(defconstant long-float-negative-infinity -1L++0)

(defconstant double-float-positive-infinity 1D++0)
(defconstant double-float-negative-infinity -1D++0)

(defconstant single-float-positive-infinity 1F++0)
(defconstant single-float-negative-infinity -1F++0)

(defconstant short-float-positive-infinity 1S++0)
(defconstant short-float-negative-infinity -1S++0)


(defun nan-p (x) (sys::nan-p x))

(defun is-nan (x) (nan-p x))

(defun infinity-p (x)
  (or (cl:= x double-float-positive-infinity)
      (cl:= x double-float-negative-infinity)))

(defun is-infinity (x)
  (infinity-p x))


;;;; Rounding Modes

(defun current-rounding-mode ()
  (unimplemented current-rounding-mode))

;;;; end of file -- lispworks.lisp
