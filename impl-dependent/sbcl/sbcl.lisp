;;;; -*- Mode: Lisp -*-

;;;; sbcl.lisp
;;;; Implementation dependent code.
;;;;
;;;; See file COPYING in the main folder for licensing information.


(in-package "CL.EXT.MATH")


(defparameter nan (sb-kernel:make-double-float -524288 0)) ; Nice trick, courtesy of Raymond Toy.
  


(defconstant long-float-positive-infinity sb-ext:long-float-positive-infinity)
(defconstant long-float-negative-infinity sb-ext:long-float-negative-infinity)

(defconstant double-float-positive-infinity sb-ext:double-float-positive-infinity)
(defconstant double-float-negative-infinity sb-ext:double-float-negative-infinity)

(defconstant single-float-positive-infinity sb-ext:single-float-positive-infinity)
(defconstant single-float-negative-infinity sb-ext:single-float-negative-infinity)

(defconstant short-float-positive-infinity sb-ext:short-float-positive-infinity)
(defconstant short-float-negative-infinity sb-ext:short-float-negative-infinity)


;;;; end of file -- sbcl.lisp
