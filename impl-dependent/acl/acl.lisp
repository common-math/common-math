;;;; -*- Mode: Lisp -*-

;;;; acl.lisp
;;;; Implementation dependent code.
;;;;
;;;; See file COPYING in the main folder for licensing information.


(in-package "CL.EXT.MATH")


(defparameter nan excl::*nan-double*)
  


(defconstant long-float-positive-infinity excl:*infinity-double*)
(defconstant long-float-negative-infinity excl:*negative-infinity-double*)

(defconstant double-float-positive-infinity excl:*infinity-double*)
(defconstant double-float-negative-infinity excl:*negative-infinity-double*)

(defconstant single-float-positive-infinity excl:*infinity-single*)
(defconstant single-float-negative-infinity excl:*negative-infinity-single*)

(defconstant short-float-positive-infinity excl:*infinity-single*)
(defconstant short-float-negative-infinity excl:*negative-infinity-single*)


;;;; end of file -- acl.lisp
