;;;; -*- Mode: Lisp -*-

;;;; common-math.lisp
;;;;
;;;; See file COPYING in the main folder for licensing information.

(in-package "CL.EXT.MATH")

;;; Conditions
;;; ==========
;;; undefined-operation

(define-condition undefined-operation (error) ; undefined-function
  ((operator :reader undefined-operation-operator ; name
	     :initarg :operator)
   (arguments :reader undefined-operation-arguments
	      :initarg :arguments)
   )
  (:report (lambda (uoc stream)
	     (format stream "Undefined operation ~S called with ~S."
		     (undefined-operation-operator uoc)
		     (undefined-operation-arguments uoc))))
  (:default-initargs :arguments () :operator nil)
  (:documentation "The Undefined Operation Condition.

An ERROR that is generated when a given operation is not (yet) defined.")
  )


;;; Constants and Variables
;;; =======================

;;; (defparameter *ignore-comparison-errors-p* nil)

(define-symbol-macro +positive-infinity+
  (coerce long-float-positive-infinity *read-default-float-format*))


(define-symbol-macro +negative-infinity+
  (coerce long-float-negative-infinity *read-default-float-format*))



;;; Generic Operations Interface
;;; ============================


;;; Predicates on numbers.
;;; ----------------------

(defun is-finite (x)
  (declare (type real x))
  (cl:< +negative-infinity+ x +positive-infinity+))


(defun finite-p (x)
  (declare (type real x))
  (is-finite x))


;;; Boolean Operations
;;; ------------------

(defgeneric .<. (x y)
  (:documentation "The dyadic 'less than' operation.")

  (:method ((x real) (y real))
   "The method on two real numbers calls CL:< directly."
   (cl:< x y))
  )


(defgeneric <. (x)
  (:documentation "The monadic 'less than' operation.

Necessary to properly implement the n-ary operators.")

  (:method ((n real))
   "The method on a real number calls CL:< directly."
   (cl:< n))
  )



(defgeneric .>. (x y)
  (:documentation "The dyadic 'greater than' operation.")

  (:method ((x real) (y real))
   "The method on two real numbers calls CL:> directly."
   (cl:> x y))
  )


(defgeneric >. (x)
  (:documentation "The monadic 'greater than' operation.

Necessary to properly implement the n-ary operators.")
  (:method ((n real)) (cl:> n))
  )


(defgeneric .=. (x y)
  (:documentation "The dyadic 'equal' operation.")

  (:method ((x number) (y number))
   "The method on two numbers calls CL:= directly."
   (cl:= x y))
  )


(defgeneric =. (x)
  (:documentation "The monadic 'equal' operation.

Necessary to properly implement the n-ary operators.")
  (:method ((n number))
   "The method on a number calls CL:= directly."
   (cl:= n))
  )


(defgeneric ./=. (x y)
  (:documentation "The dyadic 'different' operation.")
  (:method ((x number) (y number))
   "The method on two numbers calls CL:/= directly."
   (cl:/= x y))
  )


(defgeneric /=. (x)
  (:documentation "The monadic 'different' operation.

Necessary to properly implement the n-ary operators.")
  (:method ((n number))
   "The method on a number calls CL:/= directly."
   (cl:/= n))
  )


(defgeneric .<=. (x y)
  (:documentation "The dyadic 'less than or equal' operation.")
  (:method ((x real) (y real))
   "The method on two reals calls CL:<= directly."
   (cl:<= x y))
  )


(defgeneric <=. (x)
  (:documentation "The monadic 'less than or equal' operation.

Necessary to properly implement the n-ary operators.")
  (:method ((n real))
   "The method on a real calls CL:<= directly."
   (cl:<= n))
  )


(defgeneric .>=. (x y)
  (:documentation "The dyadic 'greater than or equal' operation.")
  (:method ((x real) (y real))
   "The method on two reals calls CL:>= directly."
   (cl:>= x y))
  )


(defgeneric >=. (x)
  (:documentation "The monadic 'greater than or equal' operation.

Necessary to properly implement the n-ary operators.")
  (:method ((n real))
   "The method on a real calls CL:>= directly."
   (cl:>= n))
  )


;;; Standard Dyadic and Monadic Arithmetic Operations
;;; -------------------------------------------------
;;;
;;; Note that as an implentation choice, every dyadic operation takes
;;; an optional argument that can be used as a "deposit" of the
;;; result.  This becomes useful for, e.g., matrix operations.

(defgeneric .+. (x y &optional r)
  (:documentation "The dyadic 'plus' operation.

Sums the two objects X and Y, possibly modifying R.  If R is modified than
it is returned as the result of the operation.

Arguments and Values:

X : a 'summable' object.
Y : a 'summable' object.
R : a 'summable' object.
")
  (:method ((x number) (y number) &optional r)
   (declare (ignore r))
   (let ((r (cl:+ x y)))
     (if (and (is-nan r) *error-on-nan-returning-operations*)
         (error 'floating-point-invalid-operation
              :operation '.+.
              :operands (list x y))
         r)))
  )


(defgeneric +. (x &optional r)
  (:documentation "The monadic 'plus' operation.

Sums the object X, possibly modifying R.  If R is modified than
it is returned as the result of the operation.

Arguments and Values:

X : a 'summable' object.
R : a 'summable' object.

Notes:

This generic function is necessary to properly implement the n-ary
operators.
")
  (:method ((x number) &optional r)
   (declare (ignore r))
   (cl:+ x))

  (:method ((x float) &optional r)
   (declare (ignore r))
   (let ((r (cl:+ x)))
     (if (and (is-nan r) *error-on-nan-returning-operations*)
         (error 'floating-point-invalid-operation
              :operation '+.
              :operands (list x))
         r)))
  )


(defgeneric .*. (x y &optional r)
  (:documentation "The dyadic 'times' operation.

Multiplies the two objects X and Y, possibly modifying R.  If R is modified than
it is returned as the result of the operation.

Arguments and Values:

X : a 'multipliable' object.
Y : a 'multipliable' object.
R : a 'multipliable' object.
")
  (:method ((x number) (y number) &optional r)
   (declare (ignore r))
   (let ((r (cl:* x y)))
     (if (and (is-nan r) *error-on-nan-returning-operations*)
         (error 'floating-point-invalid-operation
                :operation '.+.
                :operands (list x y))
         r)))
  )


(defgeneric *. (x &optional r)
  (:documentation "The monadic 'times' operation.

Multiplies the object X, possibly modifying R.  If R is modified than
it is returned as the result of the operation.

Arguments and Values:

X : a 'summable' object.
R : a 'summable' object.

Notes:

This generic function is necessary to properly implement the n-ary
operators.
")
  (:method ((x number) &optional r)
   (declare (ignore r))
   (cl:* x))

  (:method ((x float) &optional r)
   (declare (ignore r))
   (let ((r (cl:+ x)))
     (if (and (is-nan r) *error-on-nan-returning-operations*)
         (error 'floating-point-invalid-operation
              :operation '*.
              :operands (list x))
         r)))
  )


(defgeneric .-. (x y &optional r)
  (:documentation "The dyadic 'subtraction' operation.

Subtracts the two objects X and Y, possibly modifying R.  If R is modified than
it is returned as the result of the operation.

Arguments and Values:

X : a 'subtractable' object.
Y : a 'subtractable' object.
R : a 'subtractable' object.
")
  (:method ((x number) (y number) &optional r)
   (declare (ignore r))
   (let ((r (cl:- x y)))
     (if (and (is-nan r) *error-on-nan-returning-operations*)
         (error 'floating-point-invalid-operation
                :operation '.-.
                :operands (list x y))
         r)))
  )


(defgeneric -. (x &optional r)
  (:documentation "The monadic 'subtract' operation.

Subtracts the object X, that is, computes its 'opposite', possibly
modifying R.  If R is modified than it is returned as the result of
the operation.

Arguments and Values:

X : a 'subtractable' object.
R : a 'subtractable' object.

Notes:

This generic function is necessary to properly implement the n-ary
operators.
")
  (:method ((x number) &optional r)
   (declare (ignore r))
   (cl:- x))

  (:method ((x float) &optional r)
   (declare (ignore r))
   (let ((r (cl:+ x)))
     (if (and (is-nan r) *error-on-nan-returning-operations*)
         (error 'floating-point-invalid-operation
              :operation '-.
              :operands (list x))
         r)))
  )


(defgeneric ./. (x y &optional r)
  (:documentation "The dyadic 'divide' operation.

Multiplies the two objects X and Y, possibly modifying R.  If R is modified than
it is returned as the result of the operation.

Arguments and Values:

X : a 'divisible' object.
Y : a 'divisible' object.
R : a 'divisible' object.
")
  (:method ((x number) (y number) &optional r)
   (declare (ignore r))
   (let ((r (cl:/ x y)))
     (if (and (is-nan r) *error-on-nan-returning-operations*)
         (error 'floating-point-invalid-operation
                :operation './.
                :operands (list x y))
         r)))
  )


(defgeneric /. (x &optional r)
  (:documentation "The monadic 'divide' operation.

Divides the object X, that is, computes its 'reciprocal', possibly
modifying R.  If R is modified than it is returned as the result of
the operation.

Arguments and Values:

X : a 'divisible' object.
R : a 'divisible' object.

Notes:

This generic function is necessary to properly implement the n-ary
operators.
")
  (:method ((x number) &optional r)
   (declare (ignore r))
   (cl:/ x))

  (:method ((x float) &optional r)
   (declare (ignore r))
   (let ((r (cl:+ x)))
     (if (and (is-nan r) *error-on-nan-returning-operations*)
         (error 'floating-point-invalid-operation
              :operation '/.
              :operands (list x))
         r)))
  )


;;; Redefined Common Lisp N-adic operators
;;; --------------------------------------

;;; TODO: add other compiler macros and/or move all of them out to a
;;; separate file.

#+with-list-length
(defun + (&rest args)
  (if (null args)
      (cl:+)
      (let ((n-args (list-length args)))
        (declare (type fixnum n-args))
        (cond ((cl:= n-args 1)
               (+. (first args)))
              ((cl:= n-args 2)
               (.+. (first args) (second args)))
              (t (.+. (first args) (apply #'+ (rest args))))
              ))))


(defun + (&rest args)
  "The + Function.

Returns the sum of numbers, performing any necessary type conversions
in the process. If no numbers are supplied, 0 is returned.

Notes:

This function shadows the CL:+ function, and may signal more
error types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the
ANSI standard.
"
  (if (null args)
      (cl:+) ; Should never happen as a recursive call.

      (cond ((null (cdr args)) ; One arg.
             (+. (first args)))

            ((null (cddr args)) ; Two args.
             (.+. (first args) (second args)))

            (t ; More than two args.
             (.+. (first args) (apply #'+ (rest args))))
            )))

#+with-list-length
(define-compiler-macro + (&rest args)
  (if (null args)
      '(cl:+)
      (let ((n-args (list-length args)))
        (cond ((cl:= n-args 1)
               `(+. ,(first args)))
              ((cl:= n-args 2)
               `(.+. ,(first args) ,(second args)))
              (t
               `(.+. ,(first args) (+ ,@(rest args))))
              )))
  )

(define-compiler-macro + (&rest args)
  (if (null args)
      '(cl:+)
      (cond ((null (cdr args)) ; One arg.
             `(+. ,(first args)))

            ((null (cddr args)) ; Two args.
             `(+ ,(first args) ,(second args)))

            (t ; More than two args.
             `(.+. ,(first args) (+ ,@(rest args))))
            )))


(defun - (arg1 &rest args)
  "The - Function.

Returns the subtraction of numbers, performing any necessary type
conversions in the process. If no numbers are supplied, 0 is
returned.The function - performs arithmetic subtraction and negation.

If only one number is supplied, the negation of that number is
returned.

If more than one argument is given, it subtracts all of the
subtrahends from the minuend and returns the result.

The function - performs necessary type conversions.

Notes:

This shadows the CL:- function, and may signal more error
types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the ANSI
standard.
"
  (if (null args)
      (-. arg1)
      (if (null (cdr args))
          (.-. arg1 (first args))
          (.-. arg1 (apply #'+ args)))))

(define-compiler-macro - (arg1 &rest args)
  (if (null args)
      `(-. ,arg1)
      (if (null (cdr args))
          `(.-. ,arg1 ,(first args))
          `(.-. ,arg1 (+ ,@(rest args))))))


(defun * (&rest args)
  "The * Function.

Returns the product of numbers, performing any necessary type
conversions in the process. If no numbers are supplied, 1 is returned.

Notes:

This function shadows the CL:* function, and may signal more
error types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the
ANSI standard.
"
  (if (null args)
      (cl:*)
      (cond ((null (cdr args))
             (*. (first args)))

            ((null (cddr args))
             (.*. (first args) (second args)))

            (t
             (.*. (first args) (apply #'* (rest args))))
            )))

(define-compiler-macro * (&rest args)
  (if (null args)
      `(cl:*)
      (cond ((null (cdr args))
             `(*. ,(first args)))
            ((null (cddr args))
             `(.*. ,(first args) ,(second args)))
            (t 
             `(.*. ,(first args) (* ,@(rest args))))
            )))


(defun / (arg1 &rest args)
  "The / Function.

The function / performs division or reciprocation.

If no denominators are supplied, the function / returns the reciprocal
of number.

If at least one denominator is supplied, the function / divides the
numerator by all of the denominators and returns the resulting
quotient.

If each argument is either an integer or a ratio, and the result is
not an integer, then it is a ratio.

The function / performs necessary type conversions.

If any argument is a float then the rules of floating-point contagion
apply.

Notes:

This function shadows the CL:/ function, and may signal more
error types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the
ANSI standard.
"
  (if (null args)
      (/. arg1)
      (if (null (cdr args))
          (./. arg1 (first args))
          (./. arg1 (apply #'* args)))))

(define-compiler-macro / (arg1 &rest args)
  (if (null args)
      `(/. ,arg1)
      (if (null (cdr args))
          `(./. ,arg1 ,(first args))
          `(./. ,arg1 (* ,@(rest args))))))


(defun = (arg1 &rest args)
  "The = Function.

The value of = is true if all numbers are the same in value; otherwise
it is false. Two complexes are considered equal by = if their real and
imaginary parts are equal according to =.

Notes:

This function shadows the CL:= function, and may signal more
error types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the
ANSI standard.
"
  (if (null args)
      (=. arg1)
      (if (null (cdr args))
          (.=. arg1 (first args))
          (.=. arg1 (apply #'= args)))))

(define-compiler-macro = (arg1 &rest args)
  (if (null args)
      `(=. ,arg1)
      (if (null (cdr args))
          `(.=. ,arg1 ,(first args))
          `(.=. ,arg1 (= ,@(rest args))))))


(defun /= (arg1 &rest args)
  "The /= Function.

The value of /= is true if no two numbers are the same in value;
otherwise it is false.

Notes:

This function shadows the CL:/= function, and may signal more
error types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the
ANSI standard.
"
  (if (null args)
      (/=. arg1)
      (if (null (cdr args))
          (./=. arg1 (first args))
          (./=. arg1 (apply #'/= args)))))

(define-compiler-macro /= (arg1 &rest args)
  (if (null args)
      `(/=. ,arg1)
      (if (null (cdr args))
          `(./=. ,arg1 ,(first args))
          `(./=. ,arg1 (/= ,@(rest args))))))


(defun < (arg1 &rest args)
  "The < Function.

The value of < is true if the numbers are in monotonically increasing
order;  otherwise it is false.

Notes:

This function shadows the CL:< function, and may signal more
error types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the
ANSI standard.
"
  (if (null args)
      (<. arg1)
      (if (null (cdr args))
          (.<. arg1 (first args))
          (.<. arg1 (apply #'< args)))))

(define-compiler-macro < (arg1 &rest args)
  (if (null args)
      `(<. ,arg1)
      (if (null (cdr args))
          `(.<. ,arg1 ,(first args))
          `(.<. ,arg1 (< ,@(rest args))))))


(defun <= (arg1 &rest args)
 "The <= Function.

The value of <= is true if the numbers are in monotonically nondecreasing
order;  otherwise it is false.

Notes:

This function shadows the CL:<= function, and may signal more
error types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the
ANSI standard.
"
  (if (null args)
      (<=. arg1)
      (if (null (cdr args))
          (.<=. arg1 (first args))
          (.<=. arg1 (apply #'<= args)))))

(define-compiler-macro <= (arg1 &rest args)
  (if (null args)
      `(<=. ,arg1)
      (if (null (cdr args))
          `(.<=. ,arg1 ,(first args))
          `(.<=. ,arg1 (< ,@(rest args))))))


(defun > (arg1 &rest args)
 "The > Function.

The value of > is true if the numbers are in monotonically decreasing
order;  otherwise it is false.

Notes:

This function shadows the CL:> function, and may signal more
error types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the
ANSI standard.
"
  (if (null args)
      (>. arg1)
      (if (null (cdr args))
          (.>. arg1 (first args))
          (.>. arg1 (apply #'> args)))))

(define-compiler-macro > (arg1 &rest args)
  (if (null args)
      `(>. ,arg1)
      (if (null (cdr args))
          `(.>. ,arg1 ,(first args))
          `(.>. ,arg1 (> ,@(rest args))))))


(defun >= (arg1 &rest args)
 "The >= Function.

The value of >= is true if the numbers are in monotonically nonincreasing
order;  otherwise it is false.

Notes:

This function shadows the CL:>= function, and may signal more
error types than the TYPE-ERROR and ARITHMETIC-ERROR mentioned in the
ANSI standard.
"
  (if (null args)
      (>=. arg1)
      (if (null (cdr args))
          (.>=. arg1 (first args))
          (.>=. arg1 (apply #'>= args)))))

(define-compiler-macro >= (arg1 &rest args)
  (if (null args)
      `(>=. ,arg1)
      (if (null (cdr args))
          `(.>=. ,arg1 ,(first args))
          `(.>=. ,arg1 (>= ,@(rest args))))))


;;; Other Redefined Common Lisp Operators
;;; -------------------------------------

(defgeneric expt (base power &optional r)
  (:documentation "The EXPT Generic Function.

The EXPT function perform exponentiation.

Notes:

This function shadows the CL:EXPT function, and may signal more
error types than the ERRORs mentioned in the ANSI standard.
")
  (:method ((base number) (power number) &optional r)
   (declare (ignore r))
   (cl:expt base power)))


(defgeneric exp (number &optional r)
  (:documentation "The EXP Generic Function.

The EXPT function perform exponentiation.

Notes:

This function shadows the CL:EXP function, and may signal more
error types than the ERRORs mentioned in the ANSI standard.
")
  (:method ((n number) &optional r)
   (declare (ignore r))
   (cl:exp n)))


(defgeneric zerop (n)
  (:documentation "The ZEROP Generic Function.

Returns true if number is zero (integer, float, or complex);
otherwise, returns false.

Notes:

This generic function shadows the CL:ZEROP function, and may signal more
error types than the TYPE-ERROR mentioned in the ANSI standard.
")
  (:method ((n number))
   (cl:zerop n))
  )


(defgeneric gcd. (n &optional r)
  (:method ((n integer) &optional r)
   (declare (ignore r))
   (cl:gcd n))
  )


(defgeneric .gcd. (n m &optional r)
  (:documentation "The dyadic GCD operation.

Returns the greatest common divisor of N and M, possibly
modifying R.  If R is modified than it is returned as the result of
the operation.

Arguments and Values:

N : a 'divisible' object.
M : a 'divisible' object.
R : a 'divisible' object.

Notes:

This generic function is necessary to properly implement the n-ary
operators.")

  (:method ((n integer) (m integer) &optional r)
   "Invoking .GCD. on two INTEGERs falls back to invoking CL:GCD."
   (declare (ignore r))
   (cl:gcd n m))
  )


(defun gcd (&rest args)
 "The GCD Function.

Returns the greatest common divisor of integers. If only one integer
is supplied, its absolute value is returned. If no integers are given,
gcd returns 0, which is an identity for this operation.

Notes:

This function shadows the CL:GCD function, and may signal more
error types than the TYPE-ERROR mentioned in the ANSI standard.
"
  (cond ((null args) (cl:gcd))
        ((null (cdr args)) (gcd. (first args)))
        ((null (cddr args)) (.gcd. (first args) (second args)))
        (t (apply #'gcd (.gcd. (first args) (second args)) (cddr args)))))


(defgeneric lcm. (n &optional r)
  (:method ((n integer) &optional r)
   (declare (ignore r))
   (cl:lcm n))
  )


(defgeneric .lcm. (n m &optional r)
  (:documentation "The dyadic LCM operation.

Returns the least common multiple of N and M, possibly
modifying R.  If R is modified than it is returned as the result of
the operation.

Arguments and Values:

N : a 'multipliable' object.
M : a 'multipliable' object.
R : a 'multipliable' object.

Notes:

This generic function is necessary to properly implement the n-ary
operators.")

  (:method ((n integer) (m integer) &optional r)
   "Invoking .LCM. on two INTEGERs falls back to invoking CL:LCM."
   (declare (ignore r))
   (cl:lcm n m))
  )


(defun lcm (&rest args)
  "The LCM Function.

The LCM function returns the least common multiple of the integers.

Notes:

This function shadows the CL:LCM function, and may signal more
error types than the TYPE-ERROR mentioned in the ANSI standard.
"
  (cond ((null args) (cl:lcm))
        ((null (cdr args)) (lcm. (first args)))
        ((null (cddr args)) (.lcm. (first args) (second args)))
        (t (apply #'lcm (.lcm. (first args) (second args)) (cddr args)))))


(defgeneric increasing (x)
  (:documentation "The INCREASING Generic Function.

This function returns non-NIL if the argument has elements that are in
an increasing order with respect to the < function.

Methods for this generic function should respect this contract.
")
  (:method ((x sequence))
   (let ((l (length x)))
     (declare (type fixnum l)) ; Should be (integer 0)
     (case l
       (0 t)
       (1 (<. (elt x 0)))
       (t
        (loop for i from 0 below (1- l)
              for a1 = (elt x 0) then a2
              for a2 = (elt x (1+ i))
              always (.<. a1 a2))
        ))))
  )


(defgeneric non-increasing (x)
  (:documentation "The NON-INCREASING Generic Function.

This function returns non-NIL if the argument has elements that are in
an non increasing order with respect to the >= function.

Methods for this generic function should respect this contract.
")
  (:method ((x sequence))
   (let ((l (length x)))
     (declare (type fixnum l)) ; Should be (integer 0)
     (case l
       (0 t)
       (1 (>=. (elt x 0)))
       (t
        (loop for i from 0 below (1- l)
              for a1 = (elt x 0) then a2
              for a2 = (elt x (1+ i))
              always (.>=. a1 a2))
        ))))
  )


(defgeneric decreasing (x)
  (:documentation "The DECREASING Generic Function.

This function returns non-NIL if the argument has elements that are in
a decreasing order with respect to the > function.

Methods for this generic function should respect this contract.
")
  (:method ((x sequence))
   (let ((l (length x)))
     (declare (type fixnum l)) ; Should be (integer 0)
     (case l
       (0 t)
       (1 (>. (elt x 0)))
       (t
        (loop for i from 0 below (1- l)
              for a1 = (elt x 0) then a2
              for a2 = (elt x (1+ i))
              always (.>. a1 a2))
        ))))
  )


(defgeneric non-decreasing (x)
  (:documentation "The NON-DECREASING Generic Function.

This function returns non-NIL if the argument has elements that are in
a non decreasing order with respect to the <= function.

Methods for this generic function should respect this contract.
")
  (:method ((x sequence))
   (let ((l (length x)))
     (declare (type fixnum l)) ; Should be (integer 0)
     (case l
       (0 t)
       (1 (<=. (elt x 0)))
       (t
        (loop for i from 0 below (1- l)
              for a1 = (elt x 0) then a2
              for a2 = (elt x (1+ i))
              always (.<=. a1 a2))
        ))))
  )


(defgeneric sorted (x &key test)
  (:documentation "The SORTED Generic Function.

This function returns non-NIL if the argument has elements that are
sorted in order with respect to the TEST.
")
  (:method ((x sequence) &key (test #'<))
   (let ((l (length x)))
     (declare (type fixnum l)) ; Should be (integer 0)
     (case l
       (0 t)
       (1 (funcall test (elt x 0)))
       (t
        (loop for i from 0 below (1- l)
              for a1 = (elt x 0) then a2
              for a2 = (elt x (1+ i))
              always (funcall test a1 a2))
        ))))
  )


(defgeneric plusp (x)
  (:documentation "The PLUSP Generic Function.

Returns true if the argument is greater than zero; otherwise, returns
false.

Notes:

This function shadows the CL:PLUS function, and may signal more
error types than the TYPE-ERROR mentioned in the ANSI standard.
")
  (:method ((x real)) (cl:plusp x))
  )


(defgeneric minusp (x)
  (:documentation "The PLUSP Generic Function.

Returns true if the argument is less than zero; otherwise, returns
false.

Notes:

This function shadows the CL:MINUSP function, and may signal more
error types than the TYPE-ERROR mentioned in the ANSI standard.
")
  (:method ((x real)) (cl:minusp x))
  )


(defgeneric minimum (x)
  (:method ((x sequence))
   (let ((l (length x)))
     (declare (type fixnum l))
     (if (cl:plusp l)
         (loop with m = (elt x 0)
               for i from 0 below (length x)
               for ei = (elt x i)
               when (< ei m) do (setq m ei)
               finally (return m))
         (error "Empty sequence.")))))
         

(defgeneric maximum (x)
  (:method ((x sequence))
   (let ((l (length x)))
     (declare (type fixnum l))
     (if (cl:plusp l)
         (loop with m = (elt x 0)
               for i from 0 below (length x)
               for ei = (elt x i)
               when (> ei m) do (setq m ei)
               finally (return m))
         (error "Empty sequence.")))))


(defgeneric minmax (x))


(defun outer-product (x y &optional result)
  (.*/. x y result))

(defun inner-product (x y &optional result)
  (./*. x y result))


(defgeneric */ (x y &rest more-args))

(defgeneric .*/. (x y &optional result))


(defgeneric /* (x y &rest more-args))

(defgeneric ./*. (x y &optional result))


(defun outer-sum (x y &optional result)
  (.+/. x y result))

(defun inner-sum (x y &optional result)
  (./+. x y result))


(defgeneric +/ (x y &rest more-args))

(defgeneric .+/. (x y &optional result))


(defgeneric /+ (x y &rest more-args))

(defgeneric ./+. (x y &optional result))


;;;; end of file -- common-math.lisp
