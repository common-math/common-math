;;;; -*- Mode: Lisp -*-

;;;; prologue.lisp
;;;;
;;;; See file COPYING in the main folder for licensing information.


(in-package "CL.EXT.MATH")

(define-condition unimplemented-function (undefined-function)
  ()
  (:documentation "The Unimplemented Function Condition.

A condition signalled that specifically signals a shortcoming of the
library.

If caught, a message should be sent to the maintainers.
")

  (:report (lambda (uf stream)
             (format stream "Function ~A in not (yet) implemented in CL.EXT.MATH."
                     (cell-error-name uf)))))


(defmacro unimplemented (name)
  `(error 'unimplemented-function :name ',name))


;;;; end of file -- prologue.lisp
