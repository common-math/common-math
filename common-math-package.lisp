;;;; -*- Mode: Lisp -*-

;;;; common-math-package.lisp
;;;;
;;;; See file COPYING in the main folder for licensing information.

(defpackage "IT.UNIMIB.DISCO.MA.CL.EXT.MATH" (:use "CL")
  (:documentation "The Common Math Common Lisp Package.

The main package containing the symbols that comprise the generic math
operations library.  Most math library symbols form the CL package are
shadowed and re-exported.  The other symbols are mostly the hooks into
the generic function machinery for generic math.

The symbols in the API are constants, mostly covering IEEE 754 names
and values, and generic functions of fixed arity.  These functions
usually come in two flavors: monadic and dyadic. They have names
following a simple scheme: monadic functions have a single dot
indicating the single argument, and dyadic ones have two, indicating
both arguments.  E.g.,

Monadic minus: -.

Dyadic minus: .-.

All fixed arity generic functions also have an optional argument
possibly holding the resul.  This is ignored for simple arithmetic
results but is useful when writing, for example, matrix libraries that
must allocate the result, say, of a sum; one can pass the resulting
argument as the optional one.
")

  (:nicknames
   "COMMON-MATH"
   "MATH"
   "CL.MATH"
   "CL.EXTENSIONS.MATH"
   "CL.EXT.MATH")


  ;; IEEE names.

  (:export
   "NAN")

  (:export
   "LONG-FLOAT-POSITIVE-INFINITY"
   "LONG-FLOAT-NEGATIVE-INFINITY"

   "DOUBLE-FLOAT-POSITIVE-INFINITY"
   "DOUBLE-FLOAT-NEGATIVE-INFINITY"

   "SINGLE-FLOAT-POSITIVE-INFINITY"
   "SINGLE-FLOAT-NEGATIVE-INFINITY"

   "SHORT-FLOAT-POSITIVE-INFINITY"
   "SHORT-FLOAT-NEGATIVE-INFINITY"
   )

  (:export "+POSITIVE-INFINITY+" "+NEGATIVE-INFINITY+")

  (:export
   "IS-NAN"
   "NAN-P"
   
   "IS-INFINITY"
   "INFINITY-P"

   "IS-FINITE"
   "FINITE-P"
   )

  (:export
   "ROUNDING-MODES"
   )


  (:export
   "UNIT-ROUNDOFF"

   "+UNIT-ROUNDOFF-RATIONAL+"
   "+UNIT-ROUNDOFF-SHORT-RATIONAL+"
   "+UNIT-ROUNDOFF-SINGLE-RATIONAL+"
   "+UNIT-ROUNDOFF-DOUBLE-RATIONAL+"
   "+UNIT-ROUNDOFF-LONG-RATIONAL+"

   "+UNIT-ROUNDOFF-FLOAT+"
   "+UNIT-ROUNDOFF-SHORT-FLOAT+"
   "+UNIT-ROUNDOFF-SINGLE-FLOAT+"
   "+UNIT-ROUNDOFF-DOUBLE-FLOAT+"
   "+UNIT-ROUNDOFF-LONG-FLOAT+"
   )


  ;; Generic names (shadowing CL symbols).

  (:shadow "=" "+" "-" "*" "/" ">" "<" ">=" "<=" "/=")
  (:shadow "PLUSP" "MINUSP" "ZEROP")
  (:shadow "EXP" "EXPT" "GCD" "LCM")
  
  (:export "=" "+" "-" "*" "/" ">" "<" ">=" "<=" "/=")
  (:export "PLUSP" "MINUSP" "ZEROP")
  (:export "EXP" "EXPT" "GCD" "LCM")

  (:export
   "INCREASING" "NON-INCREASING"
   "DECREASING" "NON-DECREASING"
   "SORTED")
  (:export "MINIMUM" "MAXIMUM" "MINMAX")

  (:export "SUM")
  (:export "PRODUCT")

  (:export ; Fixed arity operators.
   "+." ".+."
   "*." ".*."
   "-." ".-."
   "/." "./."
   "=." ".=."
   ".<."
   ".>."
   ".<=."
   ".>=."
   "/=."
   "./=."
   )

  (:export "GCD." ".GCD." "LCM." ".LCM.")

  (:export "INNER-PRODUCT" "OUTER-PRODUCT" "DOT")
  (:export "/*" "./*.") ; Inner product.
  (:export "*/" ".*/.") ; Outer product.

  (:export "INNER-SUM" "OUTER-SUM")
  (:export "/+" "./+.") ; Inner sum.
  (:export "+/" ".+/.") ; Outer sum.

  ;; Conditions.

  (:export "UNDEFINED-OPERATION")

  ;; Utilities.

  (:export "USE-COMMON-MATH")

  )

;;;; end of file -- common-math-package.lisp
