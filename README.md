Common Lisp Extension: Math
===========================

Marco Antoniotti
See file COPYING for licensing information


DESCRIPTION
-----------

This library provides a layer of generic functions and some numerical
hooks that can be used to further "extend" Common Lisp mathematical
capabilities.

The symbols in the API are constants, mostly covering IEEE 754 names
and values, and generic functions of fixed arity.  These functions
usually come in two flavors: monadic and dyadic. They have names
following a simple scheme: monadic functions have a single dot
indicating the single argument, and dyadic ones have two, indicating
both arguments.

As an example:

* monadic minus: `-.`
* dyadic minus: `.-.`

All fixed arity generic functions also have an optional argument
possibly holding the resul.  This is ignored for simple arithmetic
results but is useful when writing, for example, matrix libraries that
must allocate the result, say, of a sum; one can pass the resulting
argument as the optional one.


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.
