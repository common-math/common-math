;;;; -*- Mode: Lisp -*-

;;;; utilities.lisp
;;;;
;;;; See file COPYING in the main folder for licensing information.


(in-package "CL.EXT.MATH")


(defvar *cl-pkg-shadowed-symbols*
  (append '(= + - * / > < >= <= /=)
          '(plusp minusp zerop)
          '(exp expt gcd lcm)))


(defun use-common-math (&key
                        (in *package*)
                        &aux
                        (cmpkg (find-package "CL.EXT.MATH")))
  "Uses the \"CL.EXT.MATH\" package.

This function is necessary to properly use the \"CL.EXT.MATH\" in
another package.  The \"CL.EXT.MATH\" package shadows several standard
\"COMMON-LISP\" symbols; in order to use it, such symbols must be
properly SHADOWING-IMPORTed in the target package.  The target package
is the keyword variable IN (defaulting to *PACKAGE*).
"
  (unless (eq in cmpkg)
    (shadowing-import *cl-pkg-shadowed-symbols* in)
    (use-package cmpkg in)
    ))


;;;; end of file -- utilities.lisp
